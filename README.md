There are no typos on the master branch (hopefully), there are other branches to explore!

Check the repo webpage and click "branches" and checkout those branches to find issues.

After pushing up your fixes, merge them in using the link `git push` gives you.

Hint, different partners should work on different branches simultaneously to finish faster.
